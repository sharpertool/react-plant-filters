import { resolve } from 'path'
import merge from 'webpack-merge'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'

import * as partials from './partials'

export const paths = {
  dev: resolve(__dirname, '../lib'),
  prod: resolve(__dirname, '../lib'),
  analyze: resolve(__dirname, '../dist.analyze'),
}

const libraryName = 'react-plant-filters'
const outputFile = libraryName + '.js'

export default ({ env, options }) => {

  if (options.verbose) {
    console.log(`Generating base build for ${ env.stage }`)
  }

  const isDev = /hot|dev/.test(env.stage)

  const strategy = {
    'module.rules': 'append',
  }

  return merge.strategy(strategy)({
      entry: resolve(__dirname, '../src/index.js'),
      mode: options.mode,
      output: {
        path: paths[env.stage],
        filename: outputFile,
        library: libraryName,
        libraryTarget: 'umd',
        umdNamedDefine: true,
      },
      externals: {
        react: {
          commonjs: 'react',
          commonjs2: 'react',
          amd: 'React',
          root: 'React',
        },
        'react-dom': {
          commonjs: 'react-dom',
          commonjs2: 'react-dom',
          amd: 'ReactDOM',
          root: 'ReactDOM',
        },
      },
      module: {
        rules: [
          {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
          },
          {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'eslint-loader',
          },
        ],
      },
      resolve: {
        alias: {
          'src': resolve(__dirname, '../src'),
        },
      },
      plugins: [
        new MiniCssExtractPlugin({
          filename: libraryName + '.css',
        }),
      ],
    },
    partials.loadCSS({ isDev: isDev }),
    partials.loadSCSS({ isDev: isDev }),
    partials.loadFonts(),
  )
}
