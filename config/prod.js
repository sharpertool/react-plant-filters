import merge from 'webpack-merge'

import baseConfigFunc from './base'

export default ({env, options}) => {

  console.log('Prod environment is ', env)

  const strategy = {
    'output.path': 'replace',
    'output.filename': 'replace',
    'output.publicPath': 'replace',
    'module.rules': 'append',
    'plugins': 'append',
  }

  const baseConfig = baseConfigFunc({env, options})

  return merge.strategy(strategy)(
    baseConfig,
  )
}
